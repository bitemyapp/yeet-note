use std::cmp::Ordering;
use std::collections::VecDeque;
use std::path::Path;
use std::path::PathBuf;

use failure::Fail;
use failure::ResultExt;
use try_from::TryFrom;
use walkdir::WalkDir;

use crate::fs::Entry;
use crate::fs::Metadata;
use crate::try_opt;
//re-export the result stuff.
pub use crate::scanner::result::*;

pub mod demo;
pub mod result;

#[cfg(test)]
mod tests;

struct DirScanOpts {
    filter_predicate: Option<Box<dyn FnMut(&walkdir::DirEntry) -> bool>>,
    min_depth: usize,
    append_leave_dir: bool,
}

impl DirScanOpts {
    fn new() -> Self {
        DirScanOpts {
            filter_predicate: None,
            min_depth: 0,
            append_leave_dir: false,
        }
    }
}

pub struct DirScan {
    walk_dir: WalkDir,
    opts: DirScanOpts,
}

impl DirScan {
    pub fn new<P: AsRef<Path>>(base_path: P) -> Self {
        let walk_dir = WalkDir::new(base_path)
            //for cast safety, it's important that we set this depth limit
            .max_depth(i64::max_value() as usize);
        DirScan {
            walk_dir,
            opts: DirScanOpts::new(),
        }
    }

    pub fn sort_by<F>(mut self, cmp: F) -> Self
    where
        F: FnMut(&walkdir::DirEntry, &walkdir::DirEntry) -> Ordering + Send + Sync + 'static,
    {
        self.walk_dir = self.walk_dir.sort_by(cmp);
        self
    }

    pub fn filter_entries<P>(mut self, pred: P) -> Self
    where
        P: FnMut(&walkdir::DirEntry) -> bool + Send + Sync + 'static,
    {
        self.opts.filter_predicate = Some(Box::new(pred));
        self
    }

    pub fn min_depth(mut self, depth: usize) -> Self {
        self.opts.min_depth = depth;
        self.walk_dir = self.walk_dir.min_depth(depth);
        self
    }

    pub fn append_leave_dir(mut self, append: bool) -> Self {
        self.opts.append_leave_dir = append;
        self
    }
}

impl IntoIterator for DirScan {
    type Item = ResEvent;
    type IntoIter = DirScanIterator;

    fn into_iter(self) -> Self::IntoIter {
        let iter = self.walk_dir.into_iter();
        DirScanIterator::new(iter, self.opts)
    }
}

pub struct DirScanIterator {
    last_depth: i64,
    stack_depth: usize,
    next_events: VecDeque<DirScanEvent>,
    iter: walkdir::IntoIter,
    opts: DirScanOpts,
    done: bool,
}

impl DirScanIterator {
    fn new(iter: walkdir::IntoIter, opts: DirScanOpts) -> Self {
        DirScanIterator {
            // min_depth determines what depth we start seeing entries at
            // for consumers, it's crucial that we emit an Entry before an EnterDir,
            // so we need to make sure we don't think we've entered a directory on the first entry
            // note that min_depth must be representable as a u64, which seems reasonable
            last_depth: opts.min_depth as i64,
            // likewise, if we want to properly LeaveDir the base directory, then we need to start
            // at the same depth.
            stack_depth: opts.min_depth,
            next_events: VecDeque::new(),
            iter,
            opts,
            done: false,
        }
    }

    fn get_underlying_next(&mut self) -> Option<walkdir::Result<walkdir::DirEntry>> {
        if let Some(ref mut pred) = self.opts.filter_predicate {
            // this loop operation is based on the one in walkdir.
            while let Some(next) = self.iter.next() {
                let entry = try_opt!(next);

                if pred(&entry) {
                    return Some(Ok(entry));
                } else if entry.file_type().is_dir() {
                    self.iter.skip_current_dir();
                }
            }
            None
        } else {
            self.iter.next()
        }
    }

    fn queue_event(&mut self, event: DirScanEvent) {
        self.next_events.push_back(event)
    }

    fn queue_dir_pops(&mut self, count: usize) {
        for _ in 0..count {
            self.next_events.push_back(DirScanEvent::LeaveDir)
        }
    }

    fn next_queued_event(&mut self) -> Option<DirScanEvent> {
        self.next_events.pop_front()
    }

    fn queue_final_events(&mut self) -> Option<ResEvent> {
        if self.opts.append_leave_dir {
            self.queue_dir_pops(self.stack_depth);
            self.next_queued_event().map(Ok)
        } else {
            None
        }
    }
}

impl Iterator for DirScanIterator {
    type Item = ResEvent;

    fn next(&mut self) -> Option<Self::Item> {
        //determine if we have a next entry. clear it if we do.
        if let Some(event) = self.next_queued_event() {
            return Some(Ok(event));
        }

        if self.done {
            return None;
        }

        //we don't have a next event. therefore, we need to advance the underlying iterator.
        //if the underlying iterator returns empty, so does this
        let walkdir_entry = match self.get_underlying_next() {
            //if we get None from the underlying iterator, shut down iteration, add any final events,
            //and start producing them
            None => {
                self.done = true;
                return self.queue_final_events();
            }
            Some(e) => e,
        };

        //now: convert whatever we got from the underlying iterator into DirScanEvent
        let entry_res = walkdir_entry
            .context(DirScanErrorKind::WalkDirError)
            .map_err(DirScanError::from)
            .and_then(try_walkdir_to_fs_entry);

        //in case of error, return it right away.
        let new_entry: Entry = try_opt!(entry_res);

        let new_depth = new_entry.depth();

        //determine what the current event is, and whether there is a known next event.
        match new_depth.cmp(&self.last_depth) {
            Ordering::Equal => {
                self.queue_event(DirScanEvent::Entry(new_entry));
            }

            Ordering::Less => {
                let diff = (self.last_depth - new_depth) as usize;
                self.stack_depth -= diff;
                self.queue_dir_pops(diff);
                self.queue_event(DirScanEvent::Entry(new_entry));
            }
            Ordering::Greater => {
                let diff = (new_depth - self.last_depth) as usize;
                assert_eq!(
                    diff, 1,
                    "directory recursion skipped a directory, depth increased by more than one"
                );
                self.stack_depth += 1;
                self.queue_event(DirScanEvent::EnterDir);
                self.queue_event(DirScanEvent::Entry(new_entry));
            }
        };

        //update
        self.last_depth = new_depth;

        self.next_queued_event().map(Ok)
    }
}

fn try_walkdir_to_fs_entry(entry: walkdir::DirEntry) -> DirScanResult<Entry> {
    //it's safe to convert the depth into an i64 like this because we configure WalkDir
    //to have a max depth of i64::max_value().
    let depth = entry.depth() as i64;
    let got_metadata = entry
        .metadata()
        .context(DirScanErrorKind::GetMetadataError)?;

    let metadata = Metadata::try_from(got_metadata)
        .context(entry.path().display().to_string())
        .context(DirScanErrorKind::GetMetadataError)?;

    let entry_type = entry.file_type();

    Ok(Entry::new(
        entry.into_path(),
        depth,
        entry_type.into(),
        metadata,
    ))
}

pub fn is_hidden(entry: &walkdir::DirEntry) -> bool {
    entry.file_name().to_string_lossy().starts_with('.')
}

pub fn sort_names(entry1: &walkdir::DirEntry, entry2: &walkdir::DirEntry) -> Ordering {
    entry1
        .file_name()
        .to_string_lossy()
        .cmp(&entry2.file_name().to_string_lossy())
}

pub fn is_target(entry: &walkdir::DirEntry) -> bool {
    entry.file_name().to_string_lossy().eq("target") && entry.depth() == 1
}
