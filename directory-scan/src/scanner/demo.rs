use super::*;

fn demo_fail() -> DirScanResult<()> {
    use crate::fs::MetadataConversionError;
    Err(MetadataConversionError::NoModifiedTimeAvailable)
        .context("this/is/a/path")
        .context(DirScanErrorKind::GetMetadataError)?
}

fn format_fail(f: &dyn Fail) -> String {
    use itertools::Itertools;
    let fmt = f
        .iter_chain()
        .format_with(" -> ", |elt, f| f(&format_args!("\"{}\"", elt)));

    format!("{}", fmt)
}

/// function to illustrate how DirScan works, and how it might be used.
pub fn walk_with_events<P: AsRef<Path>>(base_path: P) {
    //first, show off an error for an experiment
    let err = demo_fail().expect_err("somehow an error became not an error");

    println!("here is concatted version: {}", format_fail(&err));

    let scan = DirScan::new(base_path)
        .min_depth(1)
        .append_leave_dir(true)
        //        .sort_by(sort_names)
        .filter_entries(|entry| !is_hidden(entry) && !is_target(entry));

    let mut path_buf = PathBuf::from("");

    let mut last_entry: Option<Entry> = None;

    for event in scan {
        match event {
            Err(e) => println!("error: {}", format_fail(&e)),
            Ok(ev) => match ev {
                DirScanEvent::EnterDir => {
                    let entry = last_entry
                        .take()
                        .expect("entered dir before any entries appeared!");
                    path_buf.push(entry.name());
                    println!("entered dir: {}", path_buf.display());
                }
                DirScanEvent::Entry(entry) => {
                    let p = path_buf.join(entry.name());
                    println!("got entry: {} at depth: {}", p.display(), entry.depth());
                    last_entry.replace(entry);
                }
                DirScanEvent::LeaveDir => {
                    println!("leaving dir: {}", path_buf.display());
                    path_buf.pop();
                }
            },
        }
    }
}
