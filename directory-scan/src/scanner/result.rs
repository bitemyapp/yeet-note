use failure::Context;
use failure::Fail;

use crate::fs::Entry;

pub type DirScanResult<T> = Result<T, DirScanError>;
pub type ResEvent = DirScanResult<DirScanEvent>;

#[derive(Debug, PartialEq, Eq)]
pub enum DirScanEvent {
    /// the dir that has been entered is the last entry from the iterator
    EnterDir,

    /// iteration has visited an entry
    Entry(Entry),

    /// Pop up a dir.
    LeaveDir,
}

#[derive(Debug)]
pub struct DirScanError {
    inner: Context<DirScanErrorKind>,
}

impl Fail for DirScanError {
    fn cause(&self) -> Option<&Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&failure::Backtrace> {
        self.inner.backtrace()
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, Fail)]
pub enum DirScanErrorKind {
    #[fail(display = "error in directory walk")]
    WalkDirError,
    #[fail(display = "could not get metadata")]
    GetMetadataError,
}

impl std::fmt::Display for DirScanError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.inner, f)
    }
}

impl DirScanError {
    pub fn kind(&self) -> DirScanErrorKind {
        *self.inner.get_context()
    }
}

impl From<DirScanErrorKind> for DirScanError {
    fn from(kind: DirScanErrorKind) -> Self {
        DirScanError {
            inner: Context::new(kind),
        }
    }
}

impl From<Context<DirScanErrorKind>> for DirScanError {
    fn from(inner: Context<DirScanErrorKind>) -> Self {
        DirScanError { inner }
    }
}
