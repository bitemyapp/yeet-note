use std::fmt;

use failure::Context;
use failure::Fail;

use crate::fs;
use crate::fs::Entry;

pub type ScanResult<T> = Result<T, FileScanError>;

pub type ScanResultEntry<ID> = ScanResult<FileIndexEvent<ID>>;

#[derive(Debug)]
pub enum FileIndexEvent<ID: Copy> {
    Created(IndexedFile<ID>),
    Updated(IndexedFile<ID>),
    Unchanged(IndexedFile<ID>),
    Deleted(DeletedEntry<ID>),
}

#[derive(Debug)]
pub struct IndexedFile<ID: Copy> {
    pub(crate) id: ID,
    pub(crate) entry: Entry,
}

impl<ID: Copy> IndexedFile<ID> {
    pub fn new(id: ID, entry: Entry) -> Self {
        IndexedFile { id, entry }
    }

    pub fn id(&self) -> ID {
        self.id
    }

    pub fn entry(&self) -> &Entry {
        &self.entry
    }

    pub fn into_entry(self) -> Entry {
        self.entry
    }
}

#[derive(Debug)]
pub struct DeletedEntry<ID: Copy> {
    pub(crate) id: ID,
    pub(crate) parent_id: ID,
    pub(crate) name: String,
    pub(crate) entry_type: fs::EntryType,
}

impl<ID: Copy> DeletedEntry<ID> {
    pub fn new(id: ID, parent_id: ID, name: String, entry_type: fs::EntryType) -> Self {
        DeletedEntry {
            id,
            parent_id,
            name,
            entry_type,
        }
    }

    pub fn id(&self) -> ID {
        self.id
    }

    pub fn parent_id(&self) -> ID {
        self.parent_id
    }

    pub fn entry_type(&self) -> &fs::EntryType {
        &self.entry_type
    }

    pub fn name(&self) -> &str {
        self.name.as_str()
    }
}

#[derive(Debug)]
pub struct FileScanStats {
    pub creates: u64,
    pub updates: u64,
    pub deletes: u64,
    pub unchanged: u64,
}

impl FileScanStats {
    pub fn new() -> Self {
        FileScanStats {
            creates: 0,
            updates: 0,
            deletes: 0,
            unchanged: 0,
        }
    }

    pub fn tick_event<ID: Copy>(&mut self, event: &FileIndexEvent<ID>) {
        match event {
            FileIndexEvent::Created(_) => self.creates += 1,
            FileIndexEvent::Updated(_) => self.updates += 1,
            FileIndexEvent::Unchanged(_) => self.unchanged += 1,
            FileIndexEvent::Deleted(_) => self.deletes += 1,
        }
    }
}

impl fmt::Display for FileScanStats {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "creates: {}, updates: {}, deletes: {}, unchanged: {}",
            self.creates, self.updates, self.deletes, self.unchanged
        )
    }
}

#[derive(Debug)]
pub struct FileScanError {
    inner: Context<FileScanErrorKind>,
}

impl Fail for FileScanError {
    fn cause(&self) -> Option<&Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&failure::Backtrace> {
        self.inner.backtrace()
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, Fail)]
pub enum FileScanErrorKind {
    #[fail(display = "error in directory walk")]
    DirectoryScanError,

    #[fail(display = "could not get listing from file index")]
    IndexListingError,

    #[fail(display = "failed to update the index")]
    IndexUpdateError,

    #[fail(display = "failed to add new item to the index")]
    IndexCreateError,

    #[fail(display = "failed to delete item from the index")]
    IndexDeleteError,

    #[fail(display = "index failed trying to get the base directory")]
    GetIndexBaseError,
}

impl std::fmt::Display for FileScanError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.inner, f)
    }
}

impl FileScanError {
    pub fn kind(&self) -> FileScanErrorKind {
        *self.inner.get_context()
    }
}

impl From<FileScanErrorKind> for FileScanError {
    fn from(kind: FileScanErrorKind) -> Self {
        FileScanError {
            inner: Context::new(kind),
        }
    }
}

impl From<Context<FileScanErrorKind>> for FileScanError {
    fn from(inner: Context<FileScanErrorKind>) -> Self {
        FileScanError { inner }
    }
}
