use crate::fs;
use failure::Fail;
use std::fmt::Debug;
use std::path::PathBuf;

pub trait FileIndex {
    /// type for searching the index. note that it must implement Copy!
    type ID: Copy;

    type Entry: FileIndexEntry<ID = Self::ID>;

    type Error: Debug + Fail;

    fn get_base(&self) -> Result<Self::Entry, Self::Error>;

    fn list_dir(&self, id: Self::ID) -> Result<Vec<Self::Entry>, Self::Error>;

    fn delete(&self, id: Self::ID) -> Result<(), Self::Error>;

    fn create(&self, parent_id: Self::ID, entry: &fs::Entry) -> Result<Self::ID, Self::Error>;

    fn update(&self, id: Self::ID, entry: &fs::Entry) -> Result<(), Self::Error>;

    fn get_path(&self, id: Self::ID) -> Result<PathBuf, Self::Error>;
}

impl<'a, T: FileIndex> FileIndex for &'a T {
    type ID = T::ID;
    type Entry = T::Entry;
    type Error = T::Error;

    fn get_base(&self) -> Result<Self::Entry, Self::Error> {
        (*self).get_base()
    }

    fn list_dir(&self, id: Self::ID) -> Result<Vec<Self::Entry>, Self::Error> {
        (*self).list_dir(id)
    }

    fn delete(&self, id: Self::ID) -> Result<(), Self::Error> {
        (*self).delete(id)
    }

    fn create(&self, parent_id: Self::ID, entry: &fs::Entry) -> Result<Self::ID, Self::Error> {
        (*self).create(parent_id, entry)
    }

    fn update(&self, id: Self::ID, entry: &fs::Entry) -> Result<(), Self::Error> {
        (*self).update(id, entry)
    }

    fn get_path(&self, id: Self::ID) -> Result<PathBuf, Self::Error> {
        (*self).get_path(id)
    }
}

pub trait FileIndexEntry {
    type ID: Copy;

    fn id(&self) -> Self::ID;

    fn parent_id(&self) -> Self::ID;

    fn name(&self) -> &str;

    fn depth(&self) -> i64;

    fn metadata(&self) -> fs::Metadata;

    //    fn to_entry(self) -> fs::Entry;
    fn entry_type(&self) -> fs::EntryType;

    fn entry_is_updated(&self, entry: &fs::Entry) -> bool;
}

#[cfg(test)]
pub mod fake {
    use super::*;
    use crate::fs;
    use failure::Fail;

    #[derive(Debug, Fail)]
    pub enum FakeIndexError {
        #[fail(display = "simulated not found result")]
        NotFound,
        #[fail(display = "simulation of some kinda db error")]
        SomeKindaDBError,
        #[fail(display = "flagrant system error? that's not a good prize!")]
        FlagrantSystemError,
    }

    pub struct FakeFileIndex {}

    impl FakeFileIndex {
        pub fn new() -> Self {
            FakeFileIndex {}
        }
    }

    impl FileIndex for FakeFileIndex {
        type ID = i64;
        type Entry = FakeIndexEntry;
        type Error = FakeIndexError;

        fn get_base(&self) -> Result<Self::Entry, Self::Error> {
            unimplemented!()
        }

        fn list_dir(&self, id: Self::ID) -> Result<Vec<Self::Entry>, Self::Error> {
            unimplemented!()
        }

        fn delete(&self, id: Self::ID) -> Result<(), Self::Error> {
            unimplemented!()
        }

        fn create(&self, parent_id: Self::ID, entry: &fs::Entry) -> Result<Self::ID, Self::Error> {
            unimplemented!()
        }

        fn update(&self, id: Self::ID, entry: &fs::Entry) -> Result<(), Self::Error> {
            unimplemented!()
        }

        fn get_path(&self, id: <Self as FileIndex>::ID) -> Result<PathBuf, Self::Error> {
            unimplemented!()
        }
    }

    pub struct FakeIndexEntry {
        id: i64,
        name: String,
    }

    impl FileIndexEntry for FakeIndexEntry {
        type ID = i64;

        fn id(&self) -> Self::ID {
            self.id
        }

        fn parent_id(&self) -> <Self as FileIndexEntry>::ID {
            unimplemented!()
        }

        fn name(&self) -> &str {
            self.name.as_str()
        }

        fn depth(&self) -> i64 {
            unimplemented!()
        }

        fn metadata(&self) -> fs::Metadata {
            unimplemented!()
        }

        fn entry_type(&self) -> fs::EntryType {
            unimplemented!()
        }

        fn entry_is_updated(&self, entry: &fs::Entry) -> bool {
            unimplemented!()
        }
    }
}
