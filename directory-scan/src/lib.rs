use failure::ResultExt;

use crate::file_index::FileIndex;
use crate::file_index::FileIndexEntry;
pub use crate::result::*;
use crate::scanner::DirScan;
use crate::scanner::DirScanEvent;
use crate::scanner::DirScanIterator;
use crate::scanner::ResEvent;
use crate::utils::{Queue, Stack};
use std::path::Path;

pub mod file_index;
pub mod fs;
pub mod result;
pub mod scanner;
pub mod utils;

#[cfg(test)]
mod tests;

#[macro_export]
macro_rules! ret_none {
    ($e:expr) => {
        match $e {
            Some(v) => v,
            None => return None,
        }
    };
}

#[macro_export]
macro_rules! try_opt {
    ($e:expr) => {
        match $e {
            Ok(v) => v,
            Err(e) => return Some(Err(e.into())),
        }
    };
}

pub struct ScanWithIndex<F: FileIndex> {
    scanner: DirScan,
    file_index: F,
}

impl<F: FileIndex> ScanWithIndex<F> {
    pub fn new<P: AsRef<Path>>(base_dir: P, file_index: F) -> Self {
        let scanner = DirScan::new(base_dir)
            //we need to pop the base_dir from the stack to know what was deleted from it
            .append_leave_dir(true)
            //we don't want scan events for listing/entering the base dir
            .min_depth(1);

        ScanWithIndex {
            file_index,
            scanner,
        }
    }

    /// Sets (or replaces) the name filter predicate
    /// note that the string the predicate will get is a lossy translation
    pub fn filter_names<P>(mut self, mut name_filter: P) -> Self
    where
        P: FnMut(&str) -> bool + Send + Sync + 'static,
    {
        self.scanner = self.scanner.filter_entries(move |e| {
            let name_lossy = e.file_name().to_string_lossy();
            name_filter(&name_lossy)
        });
        self
    }

    /// certain calls must be made to the index before any iteration can occur.
    /// this method performs those calls, and if they succeed, return the value that can then be
    /// turned into an iterator.
    pub fn prepare(self) -> ScanResult<PreparedScanWithIndex<F>> {
        //we don't need the whole base_dir index entry for the scan stack, but we could use it for other purposes
        let index_base = self
            .file_index
            .get_base()
            .context(FileScanErrorKind::GetIndexBaseError)?;

        let index_base_listing = self
            .file_index
            .list_dir(index_base.id())
            .context(FileScanErrorKind::IndexListingError)?;

        let base_frame = ScanFrame::new(index_base.id(), index_base_listing);

        Ok(PreparedScanWithIndex {
            scanner: self.scanner,
            file_index: self.file_index,
            base_frame,
        })
    }
}

pub struct PreparedScanWithIndex<F: FileIndex> {
    scanner: DirScan,
    file_index: F,
    base_frame: ScanFrame<F::Entry>,
}

impl<F: FileIndex> IntoIterator for PreparedScanWithIndex<F> {
    type Item = ScanResultEntry<F::ID>;

    type IntoIter = ScanWithIndexIterator<F, DirScanIterator>;

    fn into_iter(self) -> Self::IntoIter {
        let iter = self.scanner.into_iter();
        ScanWithIndexIterator::new(self.file_index, iter, self.base_frame)
    }
}

pub struct ScanWithIndexIterator<F: FileIndex, I> {
    // scan_iter is parameterized so that we can feed it any iterator of events (for testing)
    file_index: F,
    scan_iter: I,
    stack: Stack<ScanFrame<F::Entry>>,
    next_items: Queue<ScanResultEntry<F::ID>>,
}

// first impl contains new()
impl<F, I> ScanWithIndexIterator<F, I>
where
    F: FileIndex,
    I: Iterator<Item = ResEvent>,
{
    fn new(file_index: F, scan_iter: I, base_frame: ScanFrame<F::Entry>) -> Self {
        ScanWithIndexIterator {
            file_index,
            scan_iter,
            stack: Stack::new(base_frame),
            next_items: Queue::new(),
        }
    }
}

impl<F, I> Iterator for ScanWithIndexIterator<F, I>
where
    F: FileIndex,
    I: Iterator<Item = ResEvent>,
{
    type Item = ScanResultEntry<F::ID>;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            //until next_items is clear, do not advance any underlying iterators.
            if let Some(next_ev) = self.next_items.pull() {
                return Some(next_ev);
            }

            let underlying_next: ResEvent = ret_none!(self.scan_iter.next());

            let next_ev = try_opt!(underlying_next.context(FileScanErrorKind::DirectoryScanError));

            match next_ev {
                DirScanEvent::LeaveDir => self.handle_leave_dir(),
                DirScanEvent::Entry(entry) => return Some(self.handle_entry(entry)),
                DirScanEvent::EnterDir => try_opt!(self.handle_enter_dir()),
            }
        }
    }
}

const STACK_POP_FAIL: &str = "attempted to access directory stack after popping the base dir";

// second impl contains functions for implementing next()
impl<F, I> ScanWithIndexIterator<F, I>
where
    F: FileIndex,
    I: Iterator<Item = ResEvent>,
{
    fn handle_leave_dir(&mut self) {
        //the base dir is pushed onto the stack at the beginning,
        // and logically all other LeaveDir events should have been preceded by an EnterDir event
        let left_dir = self.stack.pop().expect(STACK_POP_FAIL);

        //determine if there are any remaining index items in frame
        for deleted in left_dir.sorted_index_entries {
            //tell index that item is deleted
            let result = self.delete_entry(deleted);
            //and put the result in the queue
            self.next_items.add(result);
        }
    }

    fn delete_entry(&self, deleted: F::Entry) -> ScanResultEntry<F::ID> {
        self.file_index
            .delete(deleted.id())
            .context(FileScanErrorKind::IndexDeleteError)?;

        // the scan doesn't try to return the path of the deleted entry ... because the path doesn't exist anymore
        let entry = DeletedEntry::new(
            deleted.id(),
            deleted.parent_id(),
            deleted.name().to_string(),
            deleted.entry_type(),
        );
        Ok(FileIndexEvent::Deleted(entry))
    }

    fn handle_entry(&mut self, entry: fs::Entry) -> ScanResultEntry<F::ID> {
        /*
        technically, we could check the base_dir frame - if the last update time there is newer than this entry,
        then this entry is definitely not updated. however, in order to ensure we know which elements were deleted,
        we need to look up and remove this entry anyway.
        */
        let index_entry = self
            .stack
            .peek_mut()
            //it shouldn't be possible to see any frames after the base dir is left
            .expect(STACK_POP_FAIL)
            .take_index_entry(entry.name());

        let event = match index_entry {
            //if no entry, then this is a creation
            None => {
                let parent_id = self
                    .stack
                    .peek()
                    //at this point, we would already have panicked out!
                    .expect(STACK_POP_FAIL)
                    .parent_id();

                let id = self
                    .file_index
                    .create(parent_id, &entry)
                    .context(FileScanErrorKind::IndexCreateError)?;

                //very important - create events require explicit setting of last_id to allow entering directories to work.
                self.stack
                    .peek_mut()
                    .expect(STACK_POP_FAIL)
                    .replace_last_id(id);

                let indexed = IndexedFile::new(id, entry);
                FileIndexEvent::Created(indexed)
            }
            Some(ie) => {
                if ie.entry_is_updated(&entry) {
                    self.file_index
                        .update(ie.id(), &entry)
                        .context(FileScanErrorKind::IndexUpdateError)?;

                    let indexed = IndexedFile::new(ie.id(), entry);
                    FileIndexEvent::Updated(indexed)
                } else {
                    let indexed = IndexedFile::new(ie.id(), entry);
                    FileIndexEvent::Unchanged(indexed)
                }
            }
        };

        //get index item for entry
        Ok(event)
    }

    fn handle_enter_dir(&mut self) -> ScanResult<()> {
        let last_entry_id = self
            .stack
            .peek_mut()
            //it should not be possible to see any entries after the base dir has been popped
            //and every enter_dir is supposed to be preceded by an entry anyway
            .expect(STACK_POP_FAIL)
            .get_last_id();

        let sub_entries = self
            .file_index
            .list_dir(last_entry_id)
            .context(FileScanErrorKind::IndexListingError)?;

        let new_frame = ScanFrame::new(last_entry_id, sub_entries);

        self.stack.push(new_frame);

        Ok(())
    }
}

struct ScanFrame<E: FileIndexEntry> {
    parent_id: E::ID,

    //these need to be sorted by name. done in new().
    sorted_index_entries: Vec<E>,

    //reference to the last entry extracted from the index entries
    last_entry_id: Option<E::ID>,
}

impl<E: FileIndexEntry> ScanFrame<E> {
    fn new(parent_id: E::ID, mut entries: Vec<E>) -> Self {
        entries.sort_unstable_by(|l, r| l.name().cmp(r.name()));
        ScanFrame {
            parent_id,
            sorted_index_entries: entries,
            last_entry_id: None,
        }
    }

    fn parent_id(&self) -> E::ID {
        self.parent_id
    }

    fn take_index_entry(&mut self, name: &str) -> Option<E> {
        let result = self
            .sorted_index_entries
            .binary_search_by_key(&name, FileIndexEntry::name);

        match result {
            Ok(idx) => {
                let entry = self.sorted_index_entries.remove(idx);
                self.last_entry_id.replace(entry.id());
                Some(entry)
            }
            Err(_e) => None,
        }
    }

    fn replace_last_id(&mut self, id: E::ID) {
        self.last_entry_id.replace(id);
    }

    /// this should only be called after calling take_index_entry()
    fn get_last_id(&mut self) -> E::ID {
        self.last_entry_id
            .expect("attempted to take last file index entry before any entries have been seen!")
    }
}
