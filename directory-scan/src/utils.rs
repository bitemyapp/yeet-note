use std::collections::VecDeque;

/// Wraps a vec to present a stack-specific interface
pub struct Stack<T>(Vec<T>);

impl<T> Stack<T> {
    pub fn new(first: T) -> Self {
        Stack(vec![first])
    }

    pub fn push(&mut self, item: T) {
        self.0.push(item)
    }

    pub fn peek(&self) -> Option<&T> {
        self.0.last()
    }

    pub fn peek_mut(&mut self) -> Option<&mut T> {
        self.0.last_mut()
    }

    pub fn pop(&mut self) -> Option<T> {
        self.0.pop()
    }
}

/// Wraps a VecDeque to present a queue-specific interface.
pub struct Queue<T>(VecDeque<T>);

impl<T> Queue<T> {
    pub fn new() -> Self {
        Queue(VecDeque::new())
    }

    pub fn add(&mut self, item: T) {
        self.0.push_back(item)
    }

    pub fn peek(&self) -> Option<&T> {
        self.0.front()
    }

    pub fn peek_mut(&mut self) -> Option<&mut T> {
        self.0.front_mut()
    }

    pub fn pull(&mut self) -> Option<T> {
        self.0.pop_front()
    }
}
