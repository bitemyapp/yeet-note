use failure::Fail;
use std::fs;
use std::path::Path;
use std::path::PathBuf;
use std::time::SystemTime;
use try_from::TryFrom;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum EntryType {
    Directory,
    File,
    Symlink,
    //Unix, for example, has other types of files. we basically don't care about those for yeet-note.
    Other,
}

impl EntryType {
    pub fn from_flags(is_dir: bool, is_file: bool, is_symlink: bool) -> Self {
        if is_dir {
            EntryType::Directory
        } else if is_file {
            EntryType::File
        } else if is_symlink {
            EntryType::Symlink
        } else {
            EntryType::Other
        }
    }

    pub fn is_file(&self) -> bool {
        match self {
            EntryType::File => true,
            _ => false,
        }
    }
}

impl From<fs::FileType> for EntryType {
    fn from(ft: fs::FileType) -> Self {
        Self::from_flags(ft.is_dir(), ft.is_file(), ft.is_symlink())
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct EntryTypeFlags {
    pub is_dir: bool,
    pub is_file: bool,
    pub is_symlink: bool,
    pub is_other: bool,
}

impl From<&EntryType> for EntryTypeFlags {
    fn from(et: &EntryType) -> Self {
        let (is_dir, is_file, is_symlink, is_other) = match et {
            EntryType::Directory => (true, false, false, false),
            EntryType::File => (false, true, false, false),
            EntryType::Symlink => (false, false, true, false),
            EntryType::Other => (false, false, false, true),
        };
        EntryTypeFlags {
            is_dir,
            is_file,
            is_symlink,
            is_other,
        }
    }
}

impl From<EntryTypeFlags> for EntryType {
    fn from(etf: EntryTypeFlags) -> Self {
        Self::from_flags(etf.is_dir, etf.is_file, etf.is_symlink)
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Metadata {
    //todo: maybe use a better time rep that is compatible w/ sqlite3
    //todo: we really should have eliminated failure possibilities by this point
    modified: SystemTime,
    //this is capable of representing files up to around 9 exabytes, which seems sufficient for now.
    size_bytes: i64,
    // a note: "create time" is not actually generally available.
    // therefore we must design yn such that the "creation time" of a note is part of the metadata within.
    // assuming, ofc, that we care about time ordering.
}

impl Metadata {
    pub fn new(modified: SystemTime, size_bytes: i64) -> Self {
        Metadata {
            modified,
            size_bytes,
        }
    }

    pub fn modified(&self) -> &SystemTime {
        &self.modified
    }

    pub fn size_bytes(&self) -> i64 {
        self.size_bytes
    }
}

#[derive(Fail, Debug, PartialEq, Eq)]
pub enum MetadataConversionError {
    #[fail(display = "cannot index environments that not provide modified times")]
    NoModifiedTimeAvailable,
    #[fail(display = "cannot index files larger than ~9.2 exabytes")]
    FileSizeTooLarge,
}

impl TryFrom<fs::Metadata> for Metadata {
    type Err = MetadataConversionError;

    fn try_from(md: fs::Metadata) -> Result<Self, Self::Err> {
        let modified = md
            .modified()
            .map_err(|_| MetadataConversionError::NoModifiedTimeAvailable)?;

        let size_bytes = if md.len() > (i64::max_value() as u64) {
            return Err(MetadataConversionError::FileSizeTooLarge);
        } else {
            md.len() as i64
        };
        Ok(Metadata {
            modified,
            size_bytes,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Entry {
    pub(crate) name: String,
    pub(crate) path: PathBuf,
    pub(crate) depth: i64,
    pub(crate) entry_type: EntryType,
    pub(crate) metadata: Metadata,
}

impl Entry {
    /// Create a new entry. blank name if path is empty.
    pub fn new(path: PathBuf, depth: i64, entry_type: EntryType, metadata: Metadata) -> Self {
        //todo: figure out what to do about names in non-case-sensitive environments
        let name = path
            .file_name()
            .map(|n| n.to_string_lossy().into_owned())
            .unwrap_or_else(|| "".to_string());

        Entry {
            name,
            path,
            depth,
            entry_type,
            metadata,
        }
    }

    pub fn new_root(name: String, metadata: Metadata) -> Self {
        Entry {
            name,
            path: PathBuf::new(),
            depth: 0,
            entry_type: EntryType::Directory,
            metadata,
        }
    }

    pub fn name(&self) -> &str {
        self.name.as_str()
    }

    pub fn path(&self) -> &Path {
        self.path.as_path()
    }

    pub fn depth(&self) -> i64 {
        self.depth
    }

    pub fn entry_type(&self) -> &EntryType {
        &self.entry_type
    }

    //consume the entry and return the path
    pub fn into_path(self) -> PathBuf {
        self.path
    }

    pub fn metadata(&self) -> &Metadata {
        &self.metadata
    }
}
