build:
	cargo build

build-release:
	cargo build --release

test:
	cargo test

check:
	cargo check

build-image:
	docker build -t yeet-note .

bash-image:
	docker run -v `pwd`:/yeet-note -it yeet-note /bin/bash

docker-build:
	docker run -v `pwd`:/yeet-note -it yeet-note make build

docker-test:
	docker run -v `pwd`:/yeet-note -it yeet-note make test


.PHONY: check build build-release test
