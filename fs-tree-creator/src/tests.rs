use crate::*;

use std::io::Result;
use walkdir::DirEntry;

macro_rules! expect_next {
    ($e: ident) => {
        $e.next().expect("missing an element")
    };
}

macro_rules! expect_file {
    ($e:expr) => {
        match $e {
            FSEntryType::File(n) => n,
            other => panic!("expected to have a file, got {:?}", other),
        }
    };
}

macro_rules! expect_dir {
    ($e:expr) => {
        match $e {
            FSEntryType::Directory(n) => n,
            other => panic!("expected to have a dir, got {:?}", other),
        }
    };
}

#[test]
fn create_tempdir_tree() -> Result<()> {
    let tree: FSTree = fs_tree![
        file "hello.txt" => "HELLO WORLD";
        dir "test" => [
            file "test.txt" => "testing testing testing";
            file"test2.txt" => "testing testing testing";
        ];
    ];

    let temp_dir = tree.temp_dir()?;

    let entries: Vec<DirEntry> = walkdir::WalkDir::new(temp_dir.path())
        .sort_by(|l, r| {
            l.file_name()
                .to_string_lossy()
                .cmp(&r.file_name().to_string_lossy())
        })
        .into_iter()
        .collect::<walkdir::Result<Vec<DirEntry>>>()?;

    assert_eq!(entries.len(), 5);

    let mut entry_iter = entries.into_iter();

    let entry: DirEntry = expect_next!(entry_iter);
    assert_eq!(entry.path(), temp_dir.path());
    assert_eq!(entry.depth(), 0);

    let entry: DirEntry = expect_next!(entry_iter);
    assert_eq!(entry.file_name(), "hello.txt");
    assert_eq!(entry.depth(), 1);

    let entry: DirEntry = expect_next!(entry_iter);
    assert_eq!(entry.file_name(), "test");
    assert_eq!(entry.depth(), 1);

    let entry: DirEntry = expect_next!(entry_iter);
    assert_eq!(entry.file_name(), "test.txt");
    assert_eq!(entry.depth(), 2);

    let entry: DirEntry = expect_next!(entry_iter);
    assert_eq!(entry.file_name(), "test2.txt");
    assert_eq!(entry.depth(), 2);

    assert!(entry_iter.next().is_none());

    Ok(())
}

#[test]
fn fs_tree_empty() {
    let tree: FSTree = fs_tree![];

    assert!(tree.entries().is_empty());
}

#[test]
fn fs_tree_one_file() {
    let tree: FSTree = fs_tree![
        file "test.txt" => "nothing";
    ];

    let file = tree
        .entries()
        .first()
        .expect("missing file at head of tree");

    assert_eq!(file.name(), "test.txt");

    let contents = expect_file!(file.entry_type());
    assert_eq!(contents, "nothing");
}

#[test]
fn fs_tree_two_files() {
    let tree: FSTree = fs_tree![
        file "test0.txt" => "nothing";
        file "test1.txt" => "nothing";
    ];

    let files = tree.entries();

    assert_eq!(files.len(), 2);

    let file = files.get(0).expect("expected an entry");
    assert_eq!(file.name(), "test0.txt");
    let contents = expect_file!(file.entry_type());
    assert_eq!(contents, "nothing");

    let file = files.get(1).expect("expect a file here");
    assert_eq!(file.name(), "test1.txt");
    let contents = expect_file!(file.entry_type());
    assert_eq!(contents, "nothing");
}

#[test]
fn fs_tree_empty_dir() {
    let tree: FSTree = fs_tree![
        dir "empty" => [
        ];
    ];

    let entries = tree.entries();

    assert_eq!(entries.len(), 1);

    let dir = entries.get(0).expect("expected an entry");
    assert_eq!(dir.name(), "empty");

    let children = expect_dir!(dir.entry_type());

    assert_eq!(children.len(), 0);
}

#[test]
fn fs_tree_complex() {
    let tree: FSTree = fs_tree![
        dir "a" => [
            file "test0.txt" => "nothing";
            file "test1.txt" => "nothing";
            dir "b" => [
                file "test3.txt" => "";
                dir "c" => [
                ];
                file "test4.txt" => "";
            ];
        ];
        dir "d" => [
            file "test5.txt" => "this is a bunch of text";
        ];
    ];

    let entries = tree.entries();

    assert_eq!(entries.len(), 2);

    let entry = entries.get(0).expect("expected an entry");
    assert_eq!(entry.name(), "a");

    let a_children = expect_dir!(entry.entry_type());

    assert_eq!(a_children.len(), 3);

    let a_child = a_children.get(0).expect("expected a child here");
    assert_eq!(a_child.name(), "test0.txt");
    let contents = expect_file!(a_child.entry_type());
    assert_eq!(contents, "nothing");

    let a_child = a_children.get(1).expect("expected a child here");
    assert_eq!(a_child.name(), "test1.txt");
    let contents = expect_file!(a_child.entry_type());
    assert_eq!(contents, "nothing");

    let a_child = a_children.get(2).expect("expected a child here");
    assert_eq!(a_child.name(), "b");
    let b_children = expect_dir!(a_child.entry_type());
    assert_eq!(b_children.len(), 3);

    let b_child = b_children.get(0).expect("expected b child here");
    assert_eq!(b_child.name(), "test3.txt");
    let contents = expect_file!(b_child.entry_type());
    assert_eq!(contents, "");

    let b_child = b_children.get(1).expect("expected b child here");
    assert_eq!(b_child.name(), "c");
    let c_children = expect_dir!(b_child.entry_type());
    assert_eq!(c_children.len(), 0);

    let b_child = b_children.get(2).expect("expected b child here");
    assert_eq!(b_child.name(), "test4.txt");
    let contents = expect_file!(b_child.entry_type());
    assert_eq!(contents, "");

    let entry = entries.get(1).expect("expected an entry");
    assert_eq!(entry.name(), "d");
    let d_children = expect_dir!(entry.entry_type());
    assert_eq!(d_children.len(), 1);

    let d_child = d_children.get(0).expect("expected d child here");
    assert_eq!(d_child.name(), "test5.txt");
    let contents = expect_file!(d_child.entry_type());
    assert_eq!(contents, "this is a bunch of text");
}
