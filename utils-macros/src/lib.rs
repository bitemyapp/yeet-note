//! utilities and macros

/// Incredibly, this works. However, intellij is then completely bewildered.
#[macro_export]
macro_rules! error_with_kind {
    ($error_name: ident, $kind_name: ident) => {
        #[derive(Debug)]
        pub struct $error_name {
            inner: Context<$kind_name>,
        }

        impl failure::Fail for $error_name {
            fn cause(&self) -> Option<&failure::Fail> {
                self.inner.cause()
            }

            fn backtrace(&self) -> Option<&failure::Backtrace> {
                self.inner.backtrace()
            }
        }

        impl std::fmt::Display for $error_name {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                std::fmt::Display::fmt(&self.inner, f)
            }
        }

        impl $error_name {
            pub fn kind(&self) -> $kind_name {
                *self.inner.get_context()
            }
        }

        impl From<$kind_name> for $error_name {
            fn from(kind: $kind_name) -> Self {
                $error_name {
                    inner: Context::new(kind),
                }
            }
        }

        impl From<Context<$kind_name>> for $error_name {
            fn from(inner: Context<$kind_name>) -> Self {
                $error_name { inner }
            }
        }
    };
}

pub trait ResultConvert<T, E> {
    fn from_err<E2>(self) -> Result<T, E2>
    where
        E2: From<E>;

    fn void_res(self) -> Result<(), E>;
}

impl<T, E> ResultConvert<T, E> for Result<T, E> {
    fn from_err<E2>(self) -> Result<T, E2>
    where
        E2: From<E>,
    {
        self.map_err(From::from)
    }

    fn void_res(self) -> Result<(), E> {
        self.map(|_| {})
    }
}
