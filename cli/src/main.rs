use log::error;
use log::trace;
use std::env::current_dir;
use std::path::PathBuf;

use failure::Error;
use failure::Fallible;
use failure::ResultExt;
use structopt::StructOpt;

use directory_scan::scanner;

use crate::opts::YNCommand;
use crate::opts::YNOpt;

mod opts;

fn main() {
    let opt = YNOpt::from_args();
    stderrlog::new()
        .timestamp(stderrlog::Timestamp::Off)
        .quiet(false)
        .verbosity(5)
        .module(module_path!())
        .module("workspace")
        .init()
        .unwrap();
    if let Err(e) = main_inner(opt) {
        print_err(e);
        std::process::exit(1);
    }
}

fn print_err(e: Error) {
    use itertools::Itertools;
    let fmt = e
        .iter_chain()
        .format_with("\n* ", |elt, f| f(&format_args!("{}", elt)));

    error!("error: {}", fmt);
}

fn main_inner(opt: YNOpt) -> Fallible<()> {
    let pwd = current_dir().context("yn must be able to access the current directory!")?;

    match opt.command {
        YNCommand::Init => perform_init(pwd),
        YNCommand::Update => perform_update(pwd),
        YNCommand::ScanDir => perform_dir_scan(pwd),
    }
}

fn perform_init(pwd: PathBuf) -> Fallible<()> {
    let workspace = workspace::create::create_workspace(pwd)?;

    workspace.perform_directory_scan()?;

    Ok(())
}

fn perform_update(pwd: PathBuf) -> Fallible<()> {
    //first find the current
    let workspace = workspace::load::load_for_path(pwd)?;

    trace!(
        "got workspace at basedir: {}",
        workspace.base_path().display()
    );

    // now that we have the workspace, we can do things
    workspace.perform_directory_scan()?;

    Ok(())
}

fn perform_dir_scan(pwd: PathBuf) -> Fallible<()> {
    scanner::demo::walk_with_events(pwd);

    Ok(())
}
