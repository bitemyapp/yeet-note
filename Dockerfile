FROM rust:1.31.0

RUN apt-get update \
 && apt-get -y install build-essential \
 && apt-get clean  \
 && rm -rf /var/lib/apt/lists/*

RUN mkdir /yeet-note

VOLUME ["/yeet-note"]

WORKDIR /yeet-note
