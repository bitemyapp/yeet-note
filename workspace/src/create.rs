use std::path::Path;

use failure::Context;
use failure::Fail;
use failure::ResultExt;

use database::ConnectStr;
use database::Db;

use crate::Workspace;
use crate::WsBasePath;

pub fn create_workspace<P: AsRef<Path>>(path: P) -> Result<Workspace, CreateWorkspaceError> {
    //todo - at least make sure pwd exists
    let base_path = WsBasePath(path.as_ref().to_path_buf());

    let candidate = base_path.ws_directory();

    if candidate.exists() {
        Err(CreateWorkspaceErrorKind::AlreadyExists)?;
    }

    std::fs::create_dir(&candidate.0).context(CreateWorkspaceErrorKind::CreateDirectoryError)?;

    let db = Db::connect(&ConnectStr::Path(&candidate.push_path(super::DB_FILE)))
        .context(CreateWorkspaceErrorKind::DbSetupError)?;

    Ok(Workspace::new(base_path, db))
}

#[derive(Debug)]
pub struct CreateWorkspaceError {
    inner: Context<CreateWorkspaceErrorKind>,
}

impl Fail for CreateWorkspaceError {
    fn cause(&self) -> Option<&Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&failure::Backtrace> {
        self.inner.backtrace()
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, Fail)]
pub enum CreateWorkspaceErrorKind {
    #[fail(display = "a '.yn/' directory already exists in the current directory")]
    AlreadyExists,

    #[fail(display = "could not create '.yn/' in current directory")]
    CreateDirectoryError,

    #[fail(display = "database setup failed")]
    DbSetupError,
}

impl std::fmt::Display for CreateWorkspaceError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.inner, f)
    }
}

impl CreateWorkspaceError {
    pub fn kind(&self) -> CreateWorkspaceErrorKind {
        *self.inner.get_context()
    }
}

impl From<CreateWorkspaceErrorKind> for CreateWorkspaceError {
    fn from(kind: CreateWorkspaceErrorKind) -> Self {
        CreateWorkspaceError {
            inner: Context::new(kind),
        }
    }
}

impl From<Context<CreateWorkspaceErrorKind>> for CreateWorkspaceError {
    fn from(inner: Context<CreateWorkspaceErrorKind>) -> Self {
        CreateWorkspaceError { inner }
    }
}
