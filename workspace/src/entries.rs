use std::fmt;
use std::path::PathBuf;

use database::file_index::FileIndexKey;
use directory_scan::result::DeletedEntry;
use directory_scan::result::FileIndexEvent;
use directory_scan::result::IndexedFile;

#[derive(Debug)]
pub struct WorkspaceEntry {
    pub id: FileIndexKey,
    pub path: PathBuf,
}

impl WorkspaceEntry {
    pub fn for_indexed_entry(indexed: IndexedFile<FileIndexKey>) -> Option<Self> {
        if indexed.entry().entry_type().is_file() {
            let entry = WorkspaceEntry {
                id: indexed.id(),
                path: indexed.into_entry().into_path(),
            };
            Some(entry)
        } else {
            None
        }
    }
}

impl fmt::Display for WorkspaceEntry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} @ {}", self.id, self.path.display())
    }
}

#[derive(Debug)]
pub struct DeletedWorkspaceEntry {
    pub id: FileIndexKey,
    pub parent_id: FileIndexKey,
    //we don't care about the path for this deleted entry:
    // it's deleted, so it obviously doesn't exist in the FS anymore.
    // there's nothing to go look at
}

impl DeletedWorkspaceEntry {
    pub fn for_deleted_entry(deleted: DeletedEntry<FileIndexKey>) -> Option<Self> {
        if deleted.entry_type().is_file() {
            let entry = DeletedWorkspaceEntry {
                id: deleted.id(),
                parent_id: deleted.parent_id(),
            };
            Some(entry)
        } else {
            None
        }
    }
}

impl fmt::Display for DeletedWorkspaceEntry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}~{}", self.id, self.parent_id)
    }
}

#[derive(Debug)]
pub enum WorkspaceEntryEvent {
    Created(WorkspaceEntry),
    Modified(WorkspaceEntry),
    Deleted(DeletedWorkspaceEntry),
}

impl WorkspaceEntryEvent {
    pub fn for_scan_event(event: FileIndexEvent<FileIndexKey>) -> Option<Self> {
        match event {
            FileIndexEvent::Created(indexed) => {
                WorkspaceEntry::for_indexed_entry(indexed).map(WorkspaceEntryEvent::Created)
            }

            FileIndexEvent::Updated(indexed) => {
                WorkspaceEntry::for_indexed_entry(indexed).map(WorkspaceEntryEvent::Modified)
            }

            FileIndexEvent::Unchanged(_) => None,

            FileIndexEvent::Deleted(deleted) => {
                DeletedWorkspaceEntry::for_deleted_entry(deleted).map(WorkspaceEntryEvent::Deleted)
            }
        }
    }
}

impl fmt::Display for WorkspaceEntryEvent {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            WorkspaceEntryEvent::Created(ref i) => write!(f, "created({})", i),
            WorkspaceEntryEvent::Modified(ref i) => write!(f, "modified({})", i),
            WorkspaceEntryEvent::Deleted(ref d) => write!(f, "deleted({})", d),
        }
    }
}
