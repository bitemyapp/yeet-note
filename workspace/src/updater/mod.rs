use failure::bail;
use failure::format_err;
use failure::Fallible;
use failure::ResultExt;
use log::info;

use database::file_index::FileIndexKey;
use database::file_index::FileIndexTable;
use database::Db;
use dbthread::file_index::FileIndexSender;
use directory_scan::result::FileIndexEvent;
use directory_scan::result::FileScanStats;
use directory_scan::ScanWithIndex;

use crate::entries::WorkspaceEntryEvent;
use crate::note::NoteIndexer;
use crate::Workspace;
use crate::WsBasePath;

pub struct Updater {
    db_thread_builder: dbthread::DbThreadBuilder,
    ws_base_path: WsBasePath,
    scanner: ScanWithIndex<FileIndexSender>,
    indexer: NoteIndexer,
}

impl Updater {
    pub fn for_workspace(ws: Workspace) -> Self {
        //peel open the workspace
        let ws_base_path = ws.base_path;
        let db = ws.db;

        let (file_index_sender, file_index_handler) = dbthread::file_index::file_index_channel(10);

        let db_thread_builder = dbthread::DbThreadBuilder::new(db).add_handler(file_index_handler);

        let scanner =
            directory_scan::ScanWithIndex::new(ws_base_path.0.as_path(), file_index_sender)
                //filter out hidden files, including the .yn/ directory
                .filter_names(|name| !name.starts_with('.'));

        let indexer = NoteIndexer::new();

        Updater {
            db_thread_builder,
            ws_base_path,
            scanner,
            indexer,
        }
    }

    pub fn perform(self) -> Fallible<Workspace> {
        let mut stats = FileScanStats::new();

        //start up the db thread. this *must* happen before trying to prepare the scanner!
        let db_handle = self.db_thread_builder.run();

        let scan = self
            .scanner
            .prepare()
            .context(format_err!("failure during scan preparation"))?;

        for entry_res in scan.into_iter() {
            let next_ev = match entry_res {
                Ok(entry) => entry,
                Err(error) => {
                    super::log_error(error);
                    continue;
                }
            };

            stats.tick_event(&next_ev);

            if let Some(update) = WorkspaceEntryEvent::for_scan_event(next_ev) {
                if let Err(error) = self.indexer.handle(update) {
                    //for now, log the error
                    super::log_error(error);
                }
            }
        }

        //todo: we need to make sure that the db thread shuts down!
        let db = match db_handle.join() {
            Ok(db) => db,
            //todo: error hygiene
            Err(_) => bail!("db thread must have failed, no db available afterwards"),
        };

        info!("stats are: {}", stats);

        Ok(Workspace::new(self.ws_base_path, db))
    }
}
