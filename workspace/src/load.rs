//! Loading a workspace

use std::path::Path;

use database::ConnectStr;
use database::Db;
use failure::format_err;
use failure::Context;
use failure::Fail;
use failure::ResultExt;

use crate::Workspace;
use crate::WsBasePath;

pub fn load_for_path<P: AsRef<Path>>(path: P) -> LoadWorkspaceResult<Workspace> {
    let ws_path = find_workspace_path(path.as_ref())?;

    let db_file_path = ws_path.ws_directory().push_path(super::DB_FILE);

    let db_file_metadata = db_file_path
        .metadata()
        .context(LoadWorkspaceErrorKind::AccessError)?;

    if !db_file_metadata.is_file() {
        Err(format_err!(
            "entry {} in workspace was not a file",
            super::DB_FILE
        ))
        .context(LoadWorkspaceErrorKind::Malformed)?;
    }

    //todo: currently connect always runs migrations
    let db = Db::connect(&ConnectStr::Path(&db_file_path))
        .context(LoadWorkspaceErrorKind::DbConnectFailed)?;

    Ok(Workspace::new(ws_path, db))
}

fn find_workspace_path(start_path: &Path) -> LoadWorkspaceResult<WsBasePath> {
    let mut current_path = start_path.to_path_buf();
    loop {
        current_path.push(super::WS_DIR);
        if current_path.exists() {
            current_path.pop();
            return Ok(WsBasePath(current_path));
        }
        current_path.pop();

        //if this second pop returns false, we've consumed the entire path already
        if !current_path.pop() {
            return Err(LoadWorkspaceErrorKind::NotFound.into());
        }
    }
}

pub type LoadWorkspaceResult<T> = Result<T, LoadWorkspaceError>;

#[derive(Debug)]
pub struct LoadWorkspaceError {
    inner: Context<LoadWorkspaceErrorKind>,
}

impl Fail for LoadWorkspaceError {
    fn cause(&self) -> Option<&Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&failure::Backtrace> {
        self.inner.backtrace()
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, Fail)]
pub enum LoadWorkspaceErrorKind {
    #[fail(display = "no workspace containing the current working directory was found")]
    NotFound,
    #[fail(display = "workspace contents could not be accessed")]
    AccessError,
    #[fail(display = "workspace contents are malformed")]
    Malformed,
    #[fail(display = "failed to open database")]
    DbConnectFailed,
}

impl std::fmt::Display for LoadWorkspaceError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.inner, f)
    }
}

impl LoadWorkspaceError {
    pub fn kind(&self) -> LoadWorkspaceErrorKind {
        *self.inner.get_context()
    }
}

impl From<LoadWorkspaceErrorKind> for LoadWorkspaceError {
    fn from(kind: LoadWorkspaceErrorKind) -> Self {
        LoadWorkspaceError {
            inner: Context::new(kind),
        }
    }
}

impl From<Context<LoadWorkspaceErrorKind>> for LoadWorkspaceError {
    fn from(inner: Context<LoadWorkspaceErrorKind>) -> Self {
        LoadWorkspaceError { inner }
    }
}
