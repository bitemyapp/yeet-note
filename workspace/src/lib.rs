use std::path::{Path, PathBuf};

use failure::AsFail;
use failure::Fallible;
use log::error;

use database::Db;

pub mod create;
pub mod entries;
pub mod load;
pub mod note;
pub mod updater;

pub const WS_DIR: &str = ".yn";
pub const DB_FILE: &str = "yn.db";

/// This represents the .yn/ directory.
pub struct WsDirectory(PathBuf);

impl WsDirectory {
    /// consumes `self` and returns a modified pathbuf with `p` pushed onto it.
    pub fn push_path<P: AsRef<Path>>(self, p: P) -> PathBuf {
        let mut buf = self.0;
        buf.push(p);
        buf
    }

    pub fn exists(&self) -> bool {
        self.0.exists()
    }
}

/// this represents the base path of the workspace
#[derive(Clone)]
pub struct WsBasePath(PathBuf);

impl WsBasePath {
    pub fn ws_directory(&self) -> WsDirectory {
        WsDirectory(self.0.join(WS_DIR))
    }
}

pub struct Workspace {
    pub(crate) base_path: WsBasePath,
    pub(crate) db: Db,
}

impl Workspace {
    pub(crate) fn new(base_path: WsBasePath, db: Db) -> Workspace {
        Workspace { base_path, db }
    }

    pub fn base_path(&self) -> &Path {
        self.base_path.0.as_path()
    }

    pub fn clone_base_path(&self) -> WsBasePath {
        WsBasePath(self.base_path.0.clone())
    }

    pub fn db(&self) -> &Db {
        &self.db
    }

    pub fn perform_directory_scan(self) -> Fallible<Workspace> {
        let updater = updater::Updater::for_workspace(self);
        updater.perform()
    }
}

pub(crate) fn log_error<F: AsFail>(e: F) {
    use itertools::Itertools;
    let fmt = e
        .as_fail()
        .iter_chain()
        .format_with("\n* ", |elt, f| f(&format_args!("{}", elt)));

    error!("{}", fmt);
}
