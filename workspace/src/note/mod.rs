use failure::Fallible;
use log::trace;

use crate::entries::WorkspaceEntryEvent;
use crate::Workspace;
use crate::WsBasePath;
use database::file_index::FileIndexTable;
use std::path::PathBuf;

pub struct NoteIndexer {}

impl NoteIndexer {
    pub fn new() -> Self {
        NoteIndexer {}
    }

    pub fn handle(&self, event: WorkspaceEntryEvent) -> Fallible<()> {
        trace!("handling event: {}", event);
        //        let _file_index_table = self.ws.db().file_index();
        match event {
            WorkspaceEntryEvent::Created(entry) => {
                //we know that this file isn't in the note file index
                //we don't know whether this file has a note
                //first we need to parse the path
                //then we can use that to determine if the path is one that could be contained in an existing note
            }
            WorkspaceEntryEvent::Modified(entry) => {
                //we know that the file has been indexed before, so we just need to load the note info
                //and then
            }
            WorkspaceEntryEvent::Deleted(_) => {
                //at this point, the deletion should have cascaded fully.
                //the only thing to do, if at all, would be to re-index any parent notes
                //it would would need the path to do that though.
                //or it could use the parent id, which might be a suitable thing to pass in. let's do that.
            }
        };
        Ok(())
    }

    pub fn read_metadata_from_file(&self, parsed_path: ParsedPath) -> Fallible<()> {
        Ok(())
    }
}

pub struct NoteMetadata {
    tags: Vec<String>,
}

pub struct ParsedPath {
    path: PathBuf,
    toplevel: ContainingPathType,
}

pub enum ContainingPathType {
    //todo: put in date
    Date,
    Bare,
}

pub enum NotePathType {
    TopLevel,
    Directory,
}
