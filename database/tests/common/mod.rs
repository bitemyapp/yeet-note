use failure::Fallible;

use database::ConnectStr;
use database::Db;

pub struct TempDb {
    db: Db,
    /// this is kept as a field simply because as soon as it gets dropped the TempPath underneath it removes the file.
    _temp_file: tempfile::NamedTempFile,
}

impl TempDb {
    pub fn connect() -> Fallible<TempDb> {
        let temp_file = tempfile::NamedTempFile::new()?;
        let db = Db::connect(&ConnectStr::Path(temp_file.path()))?;
        Ok(TempDb {
            db,
            _temp_file: temp_file,
        })
    }

    pub fn db(&self) -> &Db {
        &self.db
    }
}
