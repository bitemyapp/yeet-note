use std::path::PathBuf;
use std::time::SystemTime;

use failure::Fail;

use directory_scan::fs;

use crate::connect_result::DbQueryErrorKind;
use crate::tests::get_mem_db;

use super::models::UpdateDbIndexEntry;
use super::FileIndexTable;

fn fake_metadata() -> fs::Metadata {
    fs::Metadata::new(SystemTime::now(), 0)
}

fn insert_root(table: &FileIndexTable) {
    let root_entry = fs::Entry::new_root("root".to_string(), fake_metadata());

    let new_id = table
        .insert(1, &root_entry)
        .expect("insert root should have succeeded");

    assert_eq!(new_id, 1);
}

#[test]
fn create_and_find() {
    let db = get_mem_db();
    let table = db.file_index();

    insert_root(&table);

    let next_entry = fs::Entry::new(
        PathBuf::from("next1"),
        1,
        fs::EntryType::Directory,
        fake_metadata(),
    );

    let _root_child_1 = table
        .insert(1, &next_entry)
        .expect("insert should have succeed");

    let entry2 = fs::Entry::new(
        PathBuf::from("next2"),
        1,
        fs::EntryType::Directory,
        fake_metadata(),
    );

    let root_child_2 = table
        .insert(1, &entry2)
        .expect("insert should have succeed");

    let root_children = table
        .get_children(1)
        .expect("get_children should have succeeded");

    assert_eq!(root_children.len(), 2, "got entries {:?}", root_children);

    assert_eq!(root_children[0].name, "next1");
    assert_eq!(root_children[1].name, "next2");

    let child2 = table
        .get_by_id(root_child_2)
        .expect("failed to get child 2");

    assert_eq!(child2.name, "next2");
}

#[test]
fn delete_cascades() {
    let db = get_mem_db();
    let table = db.file_index();

    insert_root(&table);

    let next_entry = fs::Entry::new(
        PathBuf::from("next1"),
        1,
        fs::EntryType::Directory,
        fake_metadata(),
    );

    let root_child_1 = table
        .insert(1, &next_entry)
        .expect("insert should have succeed");

    let entry2 = fs::Entry::new(
        PathBuf::from("next2"),
        2,
        fs::EntryType::Directory,
        fake_metadata(),
    );

    let child_2 = table
        .insert(root_child_1, &entry2)
        .expect("insert should have succeed");

    table.remove(1).expect("failed to delete root");

    let post_del_res = table
        .get_by_id(child_2)
        .expect_err("should not have been able to find child_2 after deleting root");

    assert_eq!(post_del_res.kind(), DbQueryErrorKind::QueryError);

    let cause = post_del_res
        .cause()
        .expect("there should have been a cause!");
    if let Some(diesel::result::Error::NotFound) = cause.downcast_ref::<diesel::result::Error>() {
    } else {
        panic!("got an unexpected error from the database: {:?}", cause)
    }
}

#[test]
fn can_get_path() {
    /* setup steps */
    let db = get_mem_db();
    let table = db.file_index();

    insert_root(&table);

    let next_entry = fs::Entry::new(
        PathBuf::from("next1"),
        1,
        fs::EntryType::Directory,
        fake_metadata(),
    );

    let root_child_1 = table
        .insert(1, &next_entry)
        .expect("insert should have succeed");

    let entry2 = fs::Entry::new(
        PathBuf::from("next2"),
        2,
        fs::EntryType::Directory,
        fake_metadata(),
    );

    let child_2 = table
        .insert(root_child_1, &entry2)
        .expect("insert should have succeed");

    let entry3 = fs::Entry::new(
        PathBuf::from("next3"),
        3,
        fs::EntryType::File,
        fake_metadata(),
    );

    let child_3 = table
        .insert(child_2, &entry3)
        .expect("insert should have succeed");

    /* first: test of getting ancestors */

    let ancestors = table
        .get_ancestors(child_3)
        .expect("error in get_ancestors call");

    assert_eq!(ancestors.len(), 3);

    assert_eq!(ancestors[0].name, "next3");
    assert_eq!(ancestors[1].name, "next2");
    assert_eq!(ancestors[2].name, "next1");

    /* second: test the path getting version */

    let path = table.get_path(child_3).expect("error in get_path");

    assert_eq!(path, PathBuf::from("next1/next2/next3"));
}

#[test]
fn cannot_insert_with_invalid_parent() {
    let db = get_mem_db();
    let table = db.file_index();

    insert_root(&table);

    let bad_entry = fs::Entry::new(
        PathBuf::from("fake"),
        1,
        fs::EntryType::Directory,
        fake_metadata(),
    );

    let res = table.insert(3443, &bad_entry);

    println!("{:?}", res);

    let err = res.expect_err("insert with bad parent succeded when it should have failed");

    assert_eq!(err.kind(), DbQueryErrorKind::QueryError);

    let cause = err.cause().expect("there should have been a cause!");

    if let Some(diesel::result::Error::DatabaseError(kind, _msg)) =
        cause.downcast_ref::<diesel::result::Error>()
    {
        match kind {
            diesel::result::DatabaseErrorKind::ForeignKeyViolation => (),
            _ => panic!("got some other kind of database error: {:?}", kind),
        }
    } else {
        panic!("got an unexpected error from the database: {:?}", cause)
    }
}

#[test]
fn create_and_update() {
    let db = get_mem_db();
    let table = db.file_index();

    insert_root(&table);

    let next_entry = fs::Entry::new(
        PathBuf::from("next.txt"),
        1,
        fs::EntryType::File,
        fake_metadata(),
    );

    let root_child_id = table
        .insert(1, &next_entry)
        .expect("insert should have succeed");

    let new_size: i64 = 55555;

    let modified_metadata = fs::Metadata::new(SystemTime::now(), new_size);

    let updated =
        UpdateDbIndexEntry::from_parts(root_child_id, &modified_metadata, &fs::EntryType::Symlink);

    table.do_modify(&updated).expect("do_modify failed!");

    let updated_child = table
        .get_by_id(root_child_id)
        .expect("should have been able to retrieve updated_child!");

    assert_eq!(updated_child.id, root_child_id);

    assert_eq!(updated_child.md_size_bytes, new_size);

    assert_eq!(updated_child.is_file, false);
    assert_eq!(updated_child.is_symlink, true);
}
