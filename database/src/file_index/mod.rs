use std::path::PathBuf;

use utils_macros::ResultConvert;

use diesel::prelude::*;
use diesel::result::OptionalExtension;
use diesel::sql_query;
use diesel::sql_types::BigInt;

use directory_scan::file_index::FileIndex;
use directory_scan::fs;
use file_index::models::AncestorRecord;

use crate::connect_result::DbQueryError;
use crate::connect_result::DbQueryResult;
use crate::file_index::models::DBIndexEntry;
use crate::file_index::models::NewDBIndexEntry;
use crate::file_index::models::UpdateDbIndexEntry;
use crate::Db;

pub mod models;

#[cfg(test)]
mod tests;

//if this ever needs to become a newtype wrapper,
//make sure to impl the diesel de/ser traits for it
pub type FileIndexKey = i64;

pub struct FileIndexTable<'a> {
    pub db: &'a Db,
}

impl<'a> FileIndexTable<'a> {
    pub fn insert(&self, p_id: FileIndexKey, entry: &fs::Entry) -> DbQueryResult<i64> {
        use schema::file_index::dsl::*;
        let new_entry = NewDBIndexEntry::new_from_entry(p_id, &entry);

        self.db.transaction::<i64, DbQueryError, _>(|| {
            diesel::insert_into(file_index)
                .values(new_entry)
                .execute(&*self.db.conn())?;
            file_index
                .select(id)
                .order(id.desc())
                .first(&*self.db.conn())
                .from_err()
        })
    }

    pub fn get_by_id(&self, e_id: FileIndexKey) -> DbQueryResult<DBIndexEntry> {
        use schema::file_index::dsl::*;
        file_index.find(e_id).first(&*self.db.conn()).from_err()
    }

    //complicated transaction that determines if the base entry exists and creates it if not.
    pub fn ensure_base_dir(&self) -> DbQueryResult<DBIndexEntry> {
        use schema::file_index::dsl::*;

        self.db.transaction::<DBIndexEntry, DbQueryError, _>(|| {
            let q_res: Option<DBIndexEntry> =
                file_index.find(1).first(&*self.db.conn()).optional()?;

            if let Some(base) = q_res {
                return Ok(base);
            }
            let base_entry = fs::Entry::new_root(
                "".to_string(),
                fs::Metadata::new(std::time::SystemTime::now(), 0),
            );

            let new_entry = NewDBIndexEntry::new_from_entry(1, &base_entry);

            diesel::insert_into(file_index)
                .values(new_entry)
                .execute(&*self.db.conn())?;

            file_index
                .order(id.desc())
                .first(&*self.db.conn())
                .from_err()
        })
    }

    pub fn get_children(&self, e_id: FileIndexKey) -> DbQueryResult<Vec<DBIndexEntry>> {
        use schema::file_index::dsl::*;
        file_index
            .filter(parent_id.eq(e_id).and(id.ne(e_id)))
            .order(id.asc())
            .load(&*self.db.conn())
            .from_err()
    }

    pub fn do_modify(&self, changes: &UpdateDbIndexEntry) -> DbQueryResult<()> {
        diesel::update(changes)
            .set(changes)
            .execute(&*self.db.conn())
            .void_res()
            .from_err()
    }

    pub fn modify(&self, e_id: FileIndexKey, entry: &fs::Entry) -> DbQueryResult<()> {
        self.do_modify(&UpdateDbIndexEntry::from_entry(e_id, entry))
    }

    pub fn remove(&self, e_id: FileIndexKey) -> DbQueryResult<()> {
        use schema::file_index::dsl::*;
        diesel::delete(file_index.find(e_id))
            .execute(&*self.db.conn())
            .void_res()
            .from_err()
    }

    pub fn get_ancestors(&self, e_id: FileIndexKey) -> DbQueryResult<Vec<AncestorRecord>> {
        sql_query(include_str!("rec_ancestors.sql"))
            .bind::<BigInt, i64>(e_id)
            .load::<AncestorRecord>(&*self.db.conn())
            .from_err()
    }

    pub fn get_path(&self, e_id: FileIndexKey) -> DbQueryResult<PathBuf> {
        self.get_ancestors(e_id)
            .map(|recs| itertools::rev(recs).map(|r| r.name).collect())
    }
}

impl<'a> FileIndex for FileIndexTable<'a> {
    type ID = FileIndexKey;
    type Entry = DBIndexEntry;
    //todo: error stuff
    type Error = DbQueryError;

    fn get_base(&self) -> Result<Self::Entry, Self::Error> {
        self.ensure_base_dir()
    }

    fn list_dir(&self, id: Self::ID) -> Result<Vec<Self::Entry>, Self::Error> {
        self.get_children(id)
    }

    fn delete(&self, id: Self::ID) -> Result<(), Self::Error> {
        self.remove(id)
    }

    fn create(&self, parent_id: Self::ID, entry: &fs::Entry) -> Result<Self::ID, Self::Error> {
        self.insert(parent_id, entry)
    }

    fn update(&self, id: Self::ID, entry: &fs::Entry) -> Result<(), Self::Error> {
        self.modify(id, entry)
    }

    fn get_path(&self, id: Self::ID) -> Result<PathBuf, Self::Error> {
        self.get_path(id)
    }
}
