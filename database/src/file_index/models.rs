use std::time::Duration;
use std::time::SystemTime;

use directory_scan::file_index::FileIndexEntry;
use directory_scan::fs;

use crate::file_index::FileIndexKey;
use crate::schema::file_index;

const TIME_BEFORE_EPOCH: &str = "a time should not be before the unix epoch!";

#[derive(Queryable, Debug, Identifiable)]
#[table_name = "file_index"]
pub struct DBIndexEntry {
    pub id: FileIndexKey,
    pub parent_id: FileIndexKey,
    pub name: String,
    pub depth: i64,
    //todo: is there a way to wrap this up in some sort of time type?
    pub md_modified: i64,
    pub md_size_bytes: i64,
    pub is_dir: bool,
    pub is_file: bool,
    pub is_symlink: bool,
    pub is_other: bool,
    pub first_index_time: i64,
    pub last_index_time: i64,
}

impl FileIndexEntry for DBIndexEntry {
    type ID = FileIndexKey;

    fn id(&self) -> Self::ID {
        self.id
    }

    fn parent_id(&self) -> Self::ID {
        self.parent_id
    }

    fn name(&self) -> &str {
        &self.name
    }

    fn depth(&self) -> i64 {
        self.depth
    }

    fn metadata(&self) -> fs::Metadata {
        let modified_time = SystemTime::UNIX_EPOCH + Duration::from_secs(self.md_modified as u64);
        fs::Metadata::new(modified_time, self.md_size_bytes)
    }

    fn entry_type(&self) -> fs::EntryType {
        fs::EntryType::from_flags(self.is_dir, self.is_file, self.is_symlink)
    }

    fn entry_is_updated(&self, entry: &fs::Entry) -> bool {
        if self.md_size_bytes != entry.metadata().size_bytes() {
            return true;
        }

        let entry_mod_time = entry
            .metadata()
            .modified()
            .duration_since(SystemTime::UNIX_EPOCH)
            .expect(TIME_BEFORE_EPOCH)
            .as_secs();

        let last_index_time = self.last_index_time as u64;

        let index_last_mod_time = self.md_modified as u64;

        //note that we use >= here for the same reason that git does - non-size-changing updates
        //that occur within a second of the previous update
        entry_mod_time >= last_index_time && entry_mod_time >= index_last_mod_time
    }
}

#[derive(Insertable, Debug)]
#[table_name = "file_index"]
pub struct NewDBIndexEntry<'a> {
    pub parent_id: FileIndexKey,
    pub name: &'a str,
    pub depth: i64,
    pub md_modified: i64,
    pub md_size_bytes: i64,
    pub is_dir: bool,
    pub is_file: bool,
    pub is_symlink: bool,
    pub is_other: bool,
    pub first_index_time: i64,
    pub last_index_time: i64,
}

impl<'a> NewDBIndexEntry<'a> {
    pub fn new_from_entry(parent_id: FileIndexKey, entry: &'a fs::Entry) -> Self {
        let flags = fs::EntryTypeFlags::from(entry.entry_type());

        let md_modified = entry
            .metadata()
            .modified()
            .duration_since(SystemTime::UNIX_EPOCH)
            .expect(TIME_BEFORE_EPOCH)
            .as_secs() as i64;

        let now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .expect(TIME_BEFORE_EPOCH)
            .as_secs() as i64;

        NewDBIndexEntry {
            parent_id,
            name: entry.name(),
            depth: entry.depth(),
            md_modified,
            md_size_bytes: entry.metadata().size_bytes(),
            is_dir: flags.is_dir,
            is_file: flags.is_file,
            is_symlink: flags.is_symlink,
            is_other: flags.is_other,
            first_index_time: now,
            last_index_time: now,
        }
    }
}

#[derive(AsChangeset, Identifiable, Clone, Copy)]
#[table_name = "file_index"]
pub struct UpdateDbIndexEntry {
    pub id: FileIndexKey,

    //these are all the fields that it is reasonable to modify:
    pub md_modified: i64,
    pub md_size_bytes: i64,
    pub is_dir: bool,
    pub is_file: bool,
    pub is_symlink: bool,
    pub is_other: bool,
    pub last_index_time: i64,
}

impl UpdateDbIndexEntry {
    pub fn from_parts(
        id: FileIndexKey,
        metadata: &fs::Metadata,
        entry_type: &fs::EntryType,
    ) -> Self {
        let flags = fs::EntryTypeFlags::from(entry_type);

        //todo: improve time handling!
        let md_modified = metadata
            .modified()
            .duration_since(SystemTime::UNIX_EPOCH)
            .expect(TIME_BEFORE_EPOCH)
            .as_secs() as i64;

        let now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .expect(TIME_BEFORE_EPOCH)
            .as_secs() as i64;

        UpdateDbIndexEntry {
            id,
            md_modified,
            md_size_bytes: metadata.size_bytes(),
            is_dir: flags.is_dir,
            is_file: flags.is_file,
            is_symlink: flags.is_symlink,
            is_other: flags.is_other,
            last_index_time: now,
        }
    }

    pub fn from_entry(id: FileIndexKey, entry: &fs::Entry) -> Self {
        Self::from_parts(id, entry.metadata(), entry.entry_type())
    }
}

#[derive(QueryableByName)]
#[table_name = "file_index"]
pub struct AncestorRecord {
    pub id: FileIndexKey,
    pub parent_id: FileIndexKey,
    pub name: String,
    pub depth: i64,
}
