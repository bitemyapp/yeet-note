WITH RECURSIVE
    entry_tree(id, parent_id, name, depth) AS (
        SELECT id, parent_id, name, depth FROM file_index WHERE id=?
        UNION ALL
        SELECT fi.id, fi.parent_id, fi.name, fi.depth FROM file_index fi, entry_tree et WHERE fi.id = et.parent_id AND fi.id != 1
    )
    SELECT et.id, et.parent_id, et.name, et.depth FROM entry_tree et, file_index fi WHERE et.id = fi.id;