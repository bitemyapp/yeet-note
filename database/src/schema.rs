table! {
    file_index (id) {
        id -> BigInt,
        parent_id -> BigInt,
        name -> Text,
        depth -> BigInt,
        md_modified -> BigInt,
        md_size_bytes -> BigInt,
        is_dir -> Bool,
        is_file -> Bool,
        is_symlink -> Bool,
        is_other -> Bool,
        first_index_time -> BigInt,
        last_index_time -> BigInt,
    }
}
