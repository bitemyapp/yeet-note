#![allow(proc_macro_derive_resolution_fallback)]

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
extern crate directory_scan;
extern crate failure;
extern crate utils_macros;

use std::path::Path;

use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;
use failure::ResultExt;

pub use crate::connect_result::*;
use crate::file_index::FileIndexTable;

pub mod connect_result;
pub mod file_index;
pub mod schema;

embed_migrations!("migrations/");

no_arg_sql_function!(
    last_insert_rowid,
    diesel::sql_types::BigInt,
    "Returns the ROWID of the last row insert from the database connection which invoked the function."
);

pub struct Db {
    conn: SqliteConnection,
}

pub enum ConnectStr<'a> {
    Memory,
    Path(&'a Path),
    Custom(&'a str),
}

impl<'a> ConnectStr<'a> {
    fn get_str(&self) -> DbConnectResult<&str> {
        match self {
            ConnectStr::Memory => Ok(":memory:"),
            ConnectStr::Path(p) => p
                .to_str()
                .map(Ok)
                .unwrap_or_else(|| Err(DbConnectErrorKind::NonUTF8Path.into())),
            ConnectStr::Custom(s) => Ok(s),
        }
    }
}

impl Db {
    pub fn connect(connect_str: &ConnectStr) -> DbConnectResult<Self> {
        let str_actual = connect_str.get_str()?;

        let conn =
            SqliteConnection::establish(str_actual).context(DbConnectErrorKind::ConnectFailed)?;

        let _res = diesel::sql_query("PRAGMA foreign_keys = 1")
            .execute(&conn)
            .context(DbConnectErrorKind::ForeignKeyEnableFailed)?;

        ensure_db_schema(&conn)?;

        let db = Db { conn };
        Ok(db)
    }

    pub fn conn(&self) -> &SqliteConnection {
        &self.conn
    }

    pub fn transaction<T, E, F>(&self, f: F) -> Result<T, E>
    where
        F: FnOnce() -> Result<T, E>,
        E: From<DbQueryError> + From<diesel::result::Error>,
    {
        self.conn().transaction(f)
    }

    /// Wrap this Db to use for FileIndexTable operations
    pub fn file_index(&self) -> FileIndexTable {
        FileIndexTable { db: self }
    }
}

//todo: as far as i can guess, we do have to run the migrations every time. look into the subject.
fn ensure_db_schema(conn: &SqliteConnection) -> DbConnectResult<()> {
    embedded_migrations::run(conn).context(DbConnectErrorKind::MigrationExecutionFailed)?;
    Ok(())
}

#[cfg(test)]
pub mod tests {
    use super::ConnectStr;
    use super::Db;

    pub fn get_mem_db() -> Db {
        Db::connect(&ConnectStr::Memory).expect("failed to open :memory:")
    }
}
