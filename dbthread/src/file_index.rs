use log::error;
use std::path::PathBuf;

use crossbeam_channel as cb;

use database::connect_result::DbQueryError;
use database::connect_result::DbQueryResult;
use database::file_index::models::DBIndexEntry;
use database::file_index::FileIndexKey;
use directory_scan::file_index::FileIndex;
use directory_scan::fs;

use crate::Handler;
use crate::MessageHandler;
use database::Db;

pub fn file_index_channel(limit: usize) -> (FileIndexSender, FileIndexHandler) {
    let (sender, receiver) = cb::bounded(limit);

    let sender = FileIndexSender { sender };

    let handler = FileIndexHandler { receiver };

    (sender, handler)
}

pub struct FileIndexSender {
    sender: cb::Sender<FileIndexMessage>,
}

type ResultChan<T> = cb::Sender<DbQueryResult<T>>;

pub enum FileIndexMessage {
    GetBase(ResultChan<DBIndexEntry>),
    ListDir(FileIndexKey, ResultChan<Vec<DBIndexEntry>>),

    //no back-passing on this one!
    Delete(FileIndexKey),

    Create(FileIndexKey, fs::Entry, ResultChan<FileIndexKey>),

    //update does not need to stick around for a result either
    Update(FileIndexKey, fs::Entry),

    GetPath(FileIndexKey, ResultChan<PathBuf>),
}

impl FileIndexSender {
    fn send(&self, msg: FileIndexMessage) {
        self.sender
            .send(msg)
            //todo: error handling
            .expect("could not send file index request. db thread must have shut down early!");
    }

    fn send_and_wait<T, F>(&self, f: F) -> T
    where
        F: FnOnce(cb::Sender<T>) -> FileIndexMessage,
    {
        let (s, r) = pass_back();
        let msg = f(s);
        self.send(msg);
        r.recv()
            .expect("db thread disconnected before responding to file index request!")
    }
}

impl FileIndex for FileIndexSender {
    type ID = FileIndexKey;
    type Entry = DBIndexEntry;
    type Error = DbQueryError;

    fn get_base(&self) -> Result<Self::Entry, Self::Error> {
        self.send_and_wait(FileIndexMessage::GetBase)
    }

    fn list_dir(&self, id: Self::ID) -> Result<Vec<Self::Entry>, Self::Error> {
        self.send_and_wait(move |s| FileIndexMessage::ListDir(id, s))
    }

    fn delete(&self, id: Self::ID) -> Result<(), Self::Error> {
        // let the DB thread deal with the DB error on delete.
        self.send(FileIndexMessage::Delete(id));
        Ok(())
    }

    fn create(&self, parent_id: Self::ID, entry: &fs::Entry) -> Result<Self::ID, Self::Error> {
        //todo: is there some way i can avoid cloning here?
        self.send_and_wait(move |s| FileIndexMessage::Create(parent_id, entry.clone(), s))
    }

    fn update(&self, id: Self::ID, entry: &fs::Entry) -> Result<(), Self::Error> {
        //todo: is there some way i can avoid cloning here?
        self.send(FileIndexMessage::Update(id, entry.clone()));
        Ok(())
    }

    fn get_path(&self, id: Self::ID) -> Result<PathBuf, Self::Error> {
        self.send_and_wait(move |s| FileIndexMessage::GetPath(id, s))
    }
}

pub struct FileIndexHandler {
    receiver: cb::Receiver<FileIndexMessage>,
}

impl FileIndexHandler {}

impl Handler for FileIndexHandler {
    type Message = FileIndexMessage;

    fn to_handler(self) -> MessageHandler {
        MessageHandler::FileIndex(self)
    }

    fn receiver(&self) -> &cb::Receiver<Self::Message> {
        &self.receiver
    }

    fn handle(&self, msg: Self::Message, db: &Db) {
        let file_index = db.file_index();
        match msg {
            FileIndexMessage::GetBase(res) => {
                res.send(file_index.get_base())
                    .expect("get_base - caller disconnected before response was sent");
            }
            FileIndexMessage::ListDir(e_id, res) => {
                res.send(file_index.list_dir(e_id))
                    .expect("list_dir - caller disconnected before response was sent");
            }
            FileIndexMessage::Delete(e_id) => {
                if let Err(e) = file_index.delete(e_id) {
                    error!("delete failed: {:?}", e);
                }
            }
            FileIndexMessage::Create(parent_id, entry, res) => {
                res.send(file_index.create(parent_id, &entry))
                    .expect("create - caller disconnected before response was sent");
            }
            FileIndexMessage::Update(e_id, entry) => {
                if let Err(e) = file_index.update(e_id, &entry) {
                    error!("update failed: {:?}", e);
                }
            }
            FileIndexMessage::GetPath(e_id, res) => {
                res.send(file_index.get_path(e_id))
                    .expect("get_path - caller disconnected before response was sent");
            }
        }
    }
}

fn pass_back<T>() -> (cb::Sender<T>, cb::Receiver<T>) {
    cb::bounded(1)
}
