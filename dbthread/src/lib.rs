use std::thread;
use std::thread::JoinHandle;

use crossbeam_channel as cb;

use log::trace;

use database::connect_result::DbQueryError;
use database::Db;

pub mod file_index;

pub struct DbThreadBuilder {
    db: Db,
    handlers: Vec<MessageHandler>,
    //todo
}

impl DbThreadBuilder {
    pub fn new(db: Db) -> Self {
        DbThreadBuilder {
            db,
            handlers: Vec::new(),
        }
    }

    pub fn add_handler<H: Handler>(mut self, h: H) -> Self {
        let handler = h.to_handler();
        self.handlers.push(handler);
        self
    }

    pub fn run(self) -> JoinHandle<Db> {
        trace!("DbThreadBuilder running");
        thread::spawn(move || {
            let dbt = DbThread {
                db: self.db,
                handlers: self.handlers,
            };

            dbt.run()
        })
    }
}

pub struct DbThread {
    db: Db,
    handlers: Vec<MessageHandler>,
}

impl DbThread {

    pub fn run(self) -> Db {
        trace!("DbThread running");
        let mut handlers = self.handlers;
        let db = self.db;
        (&db).transaction::<(), DbQueryError, _>(|| {
                //the main loop
                while !handlers.is_empty() {
                    let disposition = {
                        let mut select = cb::Select::new();

                        for src in handlers.iter() {
                            select = src.recv(select);
                        }

                        let oper = select.select();

                        handlers[oper.index()].handle(oper, &db)
                    };

                    if let HandlerDisposition::Disconnected(h_index) = disposition {
                        handlers.remove(h_index);
                    }
                }
                trace!("all handlers are disconnected");
                Ok(())
            })
            .expect("transaction failed! ");

        //todo: return a result here ... either alongside or wrapping the Db - use that to represent failure
        db
    }
}

pub enum MessageHandler {
    FileIndex(file_index::FileIndexHandler),
}

#[derive(PartialEq, Eq)]
enum HandlerDisposition {
    Fine,
    Disconnected(usize),
}

impl MessageHandler {
    fn recv<'a>(&'a self, mut select: cb::Select<'a>) -> cb::Select<'a> {
        match self {
            MessageHandler::FileIndex(h) => {
                select.recv(h.receiver());
            }
        }
        select
    }

    fn handle<'a>(&'a self, oper: cb::SelectedOperation<'a>, db: &Db) -> HandlerDisposition {
        let h_index = oper.index();
        match self {
            MessageHandler::FileIndex(ref h) => match oper.recv(h.receiver()) {
                Ok(msg) => {
                    h.handle(msg, db);
                    HandlerDisposition::Fine
                }
                Err(_) => HandlerDisposition::Disconnected(h_index),
            },
        }
    }
}

pub trait Handler {
    type Message;

    fn to_handler(self) -> MessageHandler;

    fn receiver(&self) -> &cb::Receiver<Self::Message>;

    fn handle(&self, msg: Self::Message, db: &Db);
}
